<?php

/**
 * https://docs.google.com/spreadsheets/d/1XdsE6EVldl8ckeeMdr2cNdbSSR3X0S-r_f7ajmtlOMI/edit#gid=396495815
 */
return [
    'all_games' => [
        [
            'slug_url' => 'Animals-Crash-Match-3',
            'category' => [
                'puzzle',
                'entertainmemnt'
            ],
            'name' => 'Animals Crash match',
            'description' => 'Match 3 puzzle game, you need to match by exchanging two pieces or by clicking on a group',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 5,
            'count_views' => 9603,
            'count_plays' => 6218,
            'path_game' => 'Animals-Crash-Match-3/HTML5/index.html',
            'thumbnail_image' => 'Animals-Crash-Match-3/HTML5/icons/icon-256.png',
        ],
        [
            'slug_url' => 'codecanyon-0BPRhEB5-mahjong-deluxe-html5-game',
            'category' => [
                'puzzle',
                'strategy'
            ],
            'name' => 'Mahjong Deluxe',
            'description' => 'This classic mahjong game will introduce you to the ancient Chinese tradition',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 6453,
            'count_plays' => 4289,
            'path_game' => 'codecanyon-0BPRhEB5-mahjong-deluxe-html5-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-0BPRhEB5-mahjong-deluxe-html5-game/thumbs/200x200_mahjong_deluxe.jpg',
        ],
        [
            'slug_url' => 'codecanyon-3M1WrqzZ-maya-ruins-html5-games-construct-3',
            'category' => [
                'puzzle',
                'entertainmemnt'
            ],
            'name' => 'Maya Ruins',
            'description' => 'A fun and challenging game that lets you rotate the pieces to reveal the hidden image',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 2560,
            'count_plays' => 2138,
            'path_game' => 'codecanyon-3M1WrqzZ-maya-ruins-html5-games-construct-3/HTML5/index.html',
            'thumbnail_image' => 'codecanyon-3M1WrqzZ-maya-ruins-html5-games-construct-3/HTML5/icons/icon-512.png',
        ],
        [
            'slug_url' => 'codecanyon-8f2CMdVq-the-sorcerer-html5-game',
            'category' => [
                'puzzle',
                'strategy'
            ],
            'name' => 'The Sorcerer',
            'description' => 'The Sorcerer was awarded as the best puzzle game in HTML5 Most Wanted contest',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 5278,
            'count_plays' => 3479,
            'path_game' => 'codecanyon-8f2CMdVq-the-sorcerer-html5-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-8f2CMdVq-the-sorcerer-html5-game/thumbs/1.jpg'
        ],
        [
            'slug_url' => 'codecanyon-9PakrZaF-speed-racer-html5-game',
            'category' => [
                'arcade',
                'sport',
                'action'
            ],
            'name' => 'Speed Racer',
            'description' => 'You are a racing driver and you must beat the clock to be number 1',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 5690,
            'count_plays' => 3367,
            'path_game' => 'codecanyon-9PakrZaF-speed-racer-html5-game/game/index.html',
            'thumbnail_image' => 'codecanyon-9PakrZaF-speed-racer-html5-game/game/share.jpg'
        ],
        [
            'slug_url' => 'codecanyon-BFbEpmpQ-sushi-matching-html5-matching-game',
            'category' => [
                'puzzle',
                'entertainmemnt'
            ],
            'name' => 'Sushi Matching',
            'description' => 'A fun and addictive matching puzzle game where you a sushi chef',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 4,
            'count_views' => 8931,
            'count_plays' => 7132,
            'path_game' => 'codecanyon-BFbEpmpQ-sushi-matching-html5-matching-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-BFbEpmpQ-sushi-matching-html5-matching-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-bN1in4Dp-bones-slasher-html5-construct-survival-game',
            'category' => [
                'action',
                'shooter'
            ],
            'name' => 'Bones Slasher',
            'description' => 'Bones Slasher is a Survival Endless Game. Welcome mighty warrior in this fantasy world!',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 4328,
            'count_plays' => 3521,
            'path_game' => 'codecanyon-bN1in4Dp-bones-slasher-html5-construct-survival-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-bN1in4Dp-bones-slasher-html5-construct-survival-game/thumbs/200x200.png'
        ],
        [
            'slug_url' => 'codecanyon-eakJSYfr-zombie-invasion-html5-survival-game',
            'category' => [
                'action',
                'shooter'
            ],
            'name' => 'Zombie Invasion',
            'description' => 'Zombie Invasion is a survival game. survive the zombie horde as long as possible.',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 3496,
            'count_plays' => 2945,
            'path_game' => 'codecanyon-eakJSYfr-zombie-invasion-html5-survival-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-eakJSYfr-zombie-invasion-html5-survival-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-EHwiY9ls-park-your-car-html5-parking-game',
            'category' => [
                'arcade',
                'strategy'
            ],
            'name' => 'Park Your Car',
            'description' => 'Be the best driver in town by parking in the designated parking spot quicky and with precision',
            'status' => 'active',
            'status_free' => 'active',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 7121,
            'count_plays' => 4812,
            'path_game' => 'codecanyon-EHwiY9ls-park-your-car-html5-parking-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-EHwiY9ls-park-your-car-html5-parking-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-GQriOZWd-html5-coloring-book-animals-html5-game',
            'category' => [
                'educational',
                'entertainment'
            ],
            'name' => 'Coloring Book Animals',
            'description' => 'Colour all the animals in the kingdom',
            'status' => 'active',
            'status_free' => 'active',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 2932,
            'count_plays' => 2193,
            'path_game' => 'codecanyon-GQriOZWd-html5-coloring-book-animals-html5-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-GQriOZWd-html5-coloring-book-animals-html5-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-mDsB74Wf-spider-solitaire-html5-solitaire-game',
            'category' => [
                'strategy',
                'entertainment'
            ],
            'name' => 'Spider Solitaire',
            'description' => 'Enjoy this stylish version of the classic Spider Solitaire!',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 5,
            'count_views' => 6623,
            'count_plays' => 4892,
            'path_game' => 'codecanyon-mDsB74Wf-spider-solitaire-html5-solitaire-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-mDsB74Wf-spider-solitaire-html5-solitaire-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-MNQASUU1-canvas-puzzle',
            'category' => [
                'educational',
                'entertainment'
            ],
            'name' => 'Canvas',
            'description' => 'a Jigsaw puzzle from your own images, just drag drop to create puzzles and enjoy',
            'status' => 'active',
            'status_free' => 'active',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 2498,
            'count_plays' => 1920,
            'path_game' => 'codecanyon-MNQASUU1-canvas-puzzle/index.html',
            'thumbnail_image' => 'codecanyon-MNQASUU1-canvas-puzzle/images/puzzle/thumbnails/scottwills_meercats.jpg'
        ],
        [
            'slug_url' => 'codecanyon-N01ou0Yq-fruit-matching-html5-matching-game',
            'category' => [
                'puzzle',
                'entertainmemnt'
            ],
            'name' => 'Fruit Matching',
            'description' => 'Another puzzle matching game, this time you matching fruits',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 4528,
            'count_plays' => 3256,
            'path_game' => 'codecanyon-N01ou0Yq-fruit-matching-html5-matching-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-N01ou0Yq-fruit-matching-html5-matching-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-NcDH5IKI-playful-kitty-html5-construct-game',
            'category' => [
                'puzzle',
                'strategy',
                'educational'
            ],
            'name' => 'Playfull Kitty',
            'description' => 'You must give the ball to the kitty oyherwise he will throw a tantrum',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 6723,
            'count_plays' => 4762,
            'path_game' => 'codecanyon-NcDH5IKI-playful-kitty-html5-construct-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-NcDH5IKI-playful-kitty-html5-construct-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-nq1y57Gu-gear-madness-html5-racing-game',
            'category' => [
                'arcade',
                'sport',
                'action'
            ],
            'name' => 'Gear Madness',
            'description' => 'You are a street racer and you must beat the locals to move up the ranks',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 5,
            'count_views' => 8781,
            'count_plays' => 6319,
            'path_game' => 'codecanyon-nq1y57Gu-gear-madness-html5-racing-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-nq1y57Gu-gear-madness-html5-racing-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-pqQemTQv-galactic-war-html5-game',
            'category' => [
                'arcade',
                'shooter'
            ],
            'name' => 'Galactic War',
            'description' => 'Galactic War is an arcade game. the goal is to survive by shooting enemy starships.',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 3481,
            'count_plays' => 2635,
            'path_game' => 'codecanyon-pqQemTQv-galactic-war-html5-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-pqQemTQv-galactic-war-html5-game/thumbs/icon_galactic_war.png'
        ],
        [
            'slug_url' => 'codecanyon-rghTtrGF-lights-html5-skill-game',
            'category' => [
                'puzzle',
                'entertainmemnt'
            ],
            'name' => 'Lights',
            'description' => 'you are an electricial and you must connect all the lights correctly otherwise you in the dark',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 4712,
            'count_plays' => 3621,
            'path_game' => 'codecanyon-rghTtrGF-lights-html5-skill-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-rghTtrGF-lights-html5-skill-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-TBdRobc9-the-bandit-hunter-html5-construct-game',
            'category' => [
                'action',
                'shooter'
            ],
            'name' => 'Bandit Hunter',
            'description' => 'The city bank is being robbed! Save the city from the evil bandits, shooting only the bad guys!',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 7883,
            'count_plays' => 5936,
            'path_game' => 'codecanyon-TBdRobc9-the-bandit-hunter-html5-construct-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-TBdRobc9-the-bandit-hunter-html5-construct-game/thumbs/200x200_the_bandit_hunter.jpg'
        ],
        [
            'slug_url' => 'codecanyon-xSFoy8GU-caveman-hunt-html5-launch-game',
            'category' => [
                'action',
                'entertainment'
            ],
            'name' => 'Caveman Hunt',
            'description' => 'Throw the caveman as far as possible and chase the mammoth! collect coins to buy power-ups',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 8346,
            'count_plays' => 7226,
            'path_game' => 'codecanyon-xSFoy8GU-caveman-hunt-html5-launch-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-xSFoy8GU-caveman-hunt-html5-launch-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'codecanyon-Y5yypYOx-miner-block-html5-puzzle-game',
            'category' => [
                'puzzle',
                'strategy'
            ],
            'name' => 'Miner Block',
            'description' => 'You must get the cart full of gold out of the mine by sliding those filled with rocks out of the way.',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 3924,
            'count_plays' => 2873,
            'path_game' => 'codecanyon-Y5yypYOx-miner-block-html5-puzzle-game/index.html',
            'thumbnail_image' => 'codecanyon-Y5yypYOx-miner-block-html5-puzzle-game/thumbs/200x200_miner_block.jpg'
        ],
        [
            'slug_url' => 'codecanyon-zf8PCGNh-jigsaw-deluxe-html5-puzzle-game',
            'category' => [
                'entertainment'
            ],
            'name' => 'Jigsaw Deluxe',
            'description' => 'Select your favorite picture and complete the jigsaw in the shortest time possible!',
            'status' => 'active',
            'status_free' => 'active',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 2389,
            'count_plays' => 1689,
            'path_game' => 'codecanyon-zf8PCGNh-jigsaw-deluxe-html5-puzzle-game/live_demo/index.html',
            'thumbnail_image' => 'codecanyon-zf8PCGNh-jigsaw-deluxe-html5-puzzle-game/thumbs/200x200.jpg'
        ],
        [
            'slug_url' => 'gameprotal01',
            'category' => [
                'arcade',
                'entertainment'
            ],
            'name' => 'The Angler',
            'description' => 'Want to go fishing without leaving the house, the angler has you covered',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 5,
            'count_views' => 7267,
            'count_plays' => 5923,
            'path_game' => 'gameprotal/01/index.html',
            'thumbnail_image' => 'gameprotal/01/icon-256.png'
        ],
        [
            'slug_url' => 'gameprotal02',
            'category' => [
                'puzzle'
            ],
            'name' => 'Gummy Block',
            'description' => 'Fun and addictive, drag the forms in the free cells and clear rows and columns in the grid!',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 3912,
            'count_plays' => 2671,
            'path_game' => 'gameprotal/02/index.html',
            'thumbnail_image' => 'gameprotal/02/sprites/200x200.jpg'
        ],
        [
            'slug_url' => 'gameprotal03',
            'category' => [
                'arcade',
                'strategy'
            ],
            'name' => 'Treasure Island',
            'description' => 'A beatifull designed island, you the pirate and must match the treasure ',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 5153,
            'count_plays' => 3874,
            'path_game' => 'gameprotal/03/index.html',
            'thumbnail_image' => 'gameprotal/03/icon-256.png'
        ],
        [
            'slug_url' => 'gameprotal04',
            'category' => [
                'arcade',
                'strategy'
            ],
            'name' => 'Aztec Treasure',
            'description' => 'Description Demo1',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 5823,
            'count_plays' => 4167,
            'path_game' => 'gameprotal/04/index.html',
            'thumbnail_image' => 'gameprotal/04/icon-256.png'
        ],
        [
            'slug_url' => 'gameprotal05',
            'category' => [
                'educational'
            ],
            'name' => 'Animals Word',
            'description' => 'One for the kids, educational and fun at the same time, learn all the animals in english',
            'status' => 'active',
            'status_free' => 'active',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 4126,
            'count_plays' => 2790,
            'path_game' => 'gameprotal/05/index.html',
            'thumbnail_image' => 'gameprotal/05/icons/icon-512.png'
        ],
        [
            'slug_url' => 'gameprotal06',
            'category' => [
                'arcade',
                'entertainment'
            ],
            'name' => 'Wack Em All',
            'description' => 'this one is simple just wack em all',
            'status' => 'active',
            'status_free' => 'active',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 3621,
            'count_plays' => 2631,
            'path_game' => 'gameprotal/06/index.html',
            'thumbnail_image' => 'gameprotal/06/sprites/200x200.jpg'
        ],
        [
            'slug_url' => 'gameprotal07',
            'category' => [
                'entertainment'
            ],
            'name' => 'Space Adventure Pinball',
            'description' => 'A Game that doesn’t need words to be introduced.',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 2549,
            'count_plays' => 1635,
            'path_game' => 'gameprotal/07/index.html',
            'thumbnail_image' => 'gameprotal/07/sprites/200x200.jpg'
        ],
        [
            'slug_url' => 'gameprotal08',
            'category' => [
                'strategy',
                'entertainment'
            ],
            'name' => 'Battleship',
            'description' => 'Battleship War is a Skill Game. Hit all the ships of your enemy and lead your fleet to the victory!',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 5,
            'count_views' => 4716,
            'count_plays' => 3128,
            'path_game' => 'gameprotal/08/index.html',
            'thumbnail_image' => 'gameprotal/08/sprites/200x200.jpg'
        ],
        [
            'slug_url' => 'gameprotal09',
            'category' => [
                'arcade',
                'sport',
                'entertainment'
            ],
            'name' => 'Mini Golf 3D',
            'description' => 'The soothing sound of chirping birds accompany you through 18 holes of this addicting mini golf.',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 5,
            'count_views' => 6378,
            'count_plays' => 4671,
            'path_game' => 'gameprotal/09/index.html',
            'thumbnail_image' => 'gameprotal/09/sprites/200x200.jpg'
        ],
        [
            'slug_url' => 'gameprotal10',
            'category' => [
                'arcade'
            ],
            'name' => 'Brick Out',
            'description' => 'The aim is to destroy all the bricks and collect any falling power-ups to move to the next level.',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 5,
            'count_views' => 7751,
            'count_plays' => 5812,
            'path_game' => 'gameprotal/10/index.html',
            'thumbnail_image' => 'gameprotal/10/sprites/200x200.jpg'
        ],
        [
            'slug_url' => 'Gold-Miner-Jack',
            'category' => [
                'puzzle',
                'entertainment'
            ],
            'name' => 'Gold Miner Jack',
            'description' => 'You are Jack the gold miner and have a quota to stick to otherwise you out of a job',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 5,
            'count_views' => 6178,
            'count_plays' => 4838,
            'path_game' => 'Gold-Miner-Jack/HTML5/index.html',
            'thumbnail_image' => 'Gold-Miner-Jack/Icons/Icon_1024.png'
        ],
        [
            'slug_url' => 'Plumber',
            'category' => [
                'puzzle',
                'entertainment'
            ],
            'name' => 'Plumber',
            'description' => 'You are a plumber and you must connect the correct pipes together to get the water to flow',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 5,
            'count_views' => 8410,
            'count_plays' => 7169,
            'path_game' => 'Plumber/HTML5/index.html',
            'thumbnail_image' => 'Plumber/Icons/Icon_1024.png'
        ],
        [
            'slug_url' => 'SpectGame',
            'category' => [
                'arcade',
                'shooter'
            ],
            'name' => 'Spec',
            'description' => 'Spect is fantastic, a dynamic shooter game in space',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'active',
            'rating' => 5,
            'count_views' => 8189,
            'count_plays' => 7356,
            'path_game' => 'SpectGame/HTML5/index.html',
            'thumbnail_image' => 'SpectGame/HTML5/images/logo-sheet0.png'
        ],
        [
            'slug_url' => 'Super-Cowboy-Run',
            'category' => [
                'arcade',
                'shooter'
            ],
            'name' => 'Super Cowboy Run',
            'description' => 'A fascinating and beautiful game in which you collect coins, lives, ammunition and kill monsters,',
            'status' => 'active',
            'status_free' => 'inactive',
            'status_popular' => 'inactive',
            'rating' => 4,
            'count_views' => 4931,
            'count_plays' => 4167,
            'path_game' => 'Super-Cowboy-Run/HTML5/index.html',
            'thumbnail_image' => 'Super-Cowboy-Run/Icons/Icon_512.png'
        ]
    ],
];
