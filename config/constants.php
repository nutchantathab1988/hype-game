<?php

return [
    'secretKey' => 'bninetech',
    'code' => 'AES-128-ECB',
    'status' => [
        'active' => 'active',
        'inactive' => 'inactive',
    ],
    'status_popular' => [
        'active' => 'active',
        'inactive' => 'inactive',
    ],
    'status_free' => [
        'active' => 'active',
        'inactive' => 'inactive',
    ],
    'position' => [
        'admin' => 'admin',
        'superadmin' => 'superadmin'
    ],
];
