<?php

namespace App\Exports;

use App\Models\TokenPlayGame;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReportTokenPlayGameExport implements FromView
{
    public $token_play_games;

    public function __construct($token_play_games)
    {
        $this->token_play_games = $token_play_games;
    }

    public function view(): View
    {
        return view('backend.pages.report-token-play-games.excel', [
            'token_play_games' => $this->token_play_games
        ]);
    }
}
