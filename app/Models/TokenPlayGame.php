<?php

namespace App\Models;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TokenPlayGame extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'token_play_games';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Scope a query to only include active partners.
     */
    public function scopeFilters(Builder $query, $partner_id, $start_date_time, $end_date_time): void
    {
        if ($partner_id) {
            $query->where('partner_id', $partner_id);
        }
        if ($start_date_time && $end_date_time) {
            $query->whereBetween('created_at', [$start_date_time, $end_date_time]);
        }
        $query->orderBy('id','Desc');
    }

    /**
     * Scope a query to only include active partners.
     */
    public function scopeActive(Builder $query): void
    {
        $query->where('status', config('constants.status.active'));
    }

    /**
     * Scope a query to only include inactive partners.
     */
    public function scopeInActive(Builder $query): void
    {
        $query->where('status', config('constants.status.inactive'));
    }

    /**
     * Get the partner that owns the comment.
     */
    public function partner(): BelongsTo
    {
        return $this->belongsTo(Partner::class);
    }

    /**
     * Get the token that owns the comment.
     */
    public function token(): BelongsTo
    {
        return $this->belongsTo(Token::class);
    }

    /**
     * Get the token that owns the comment.
     */
    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }
}
