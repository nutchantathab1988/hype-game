<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Laravel\Sanctum\HasApiTokens;

class Partner extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'partners';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'password'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * Scope a query to only include active partners.
     */
    public function scopeActive(Builder $query): void
    {
        $query->where('status', config('constants.status.active'));
    }

    /**
     * Scope a query to only include inactive partners.
     */
    public function scopeInActive(Builder $query): void
    {
        $query->where('status', config('constants.status.inactive'));
    }

    /**
     * Get the tokens for the GenerateToken.
     */
    public function generate_tokens(): HasMany
    {
        return $this->hasMany(GenerateToken::class, 'partner_id', 'id');
    }

    /**
     * Get the tokens for the Token.
     */
    public function token_games(): HasMany
    {
        return $this->hasMany(Token::class, 'partner_id', 'id');
    }

    /**
     * Get the user that user.
     */
    public function created_by(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_created_id', 'id');
    }
}
