<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class Game extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'games';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'category' => 'array',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    public function getNameLimitAttribute() {

        return Str::limit($this->name, 14);
    }

    public function scopeFilter(Builder $query, $category, $status_popular, $status_free) {

        if ($category) {
            $query->whereJsonContains('category', $category);
        }
        if ($status_popular) {
            $query->where('status_popular', $status_popular);
        }
        if ($status_free) {
            $query->where('status_free', $status_free);
        }
    }

    /**
     * Scope a query to only include active partners.
     */
    public function scopePopular(Builder $query): void
    {
        $query->where('status_popular', config('constants.status_popular.active'));
    }

    /**
     * Scope a query to only include inactive partners.
     */
    public function scopeNotPopular(Builder $query): void
    {
        $query->where('status_popular', config('constants.status_popular.inactive'));
    }

    /**
     * Scope a query to only include active partners.
     */
    public function scopeFree(Builder $query): void
    {
        $query->where('status_free', config('constants.status_free.active'));
    }

    /**
     * Scope a query to only include inactive partners.
     */
    public function scopeNotFree(Builder $query): void
    {
        $query->where('status_free', config('constants.status_free.inactive'));
    }

    /**
     * Scope a query to only include active partners.
     */
    public function scopeActive(Builder $query): void
    {
        $query->where('status', config('constants.status.active'));
    }

    /**
     * Scope a query to only include inactive partners.
     */
    public function scopeInActive(Builder $query): void
    {
        $query->where('status', config('constants.status.inactive'));
    }
}
