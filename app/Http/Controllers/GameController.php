<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\TokenPlayGame;
use Illuminate\Http\Request;

class GameController extends Controller
{
    //
    public function allGamePage(Request $request) {

        $all_games = Game::filter($request->category, $request->status_popular, $request->status_free)->active()->orderBy('id', 'Desc')->get();

        return view('all-games', [
            'all_games' => $all_games
        ]);
    }

    public function previewGamePage($slugUrl, Request $request) {

        $game = Game::where('slug_url', $slugUrl)->active()->first();

        if ($game) {

            Game::where('slug_url', $slugUrl)
                ->increment('count_views', 1);

            if (isset(session()->get('sess_subscriber')['unique_code'])) {

                TokenPlayGame::create([
                    'partner_id' => session()->get('sess_subscriber')['partner_id'],
                    'token_id' => session()->get('sess_subscriber')['token_id'],
                    'type' => 'preview',
                    'game_id' => $game->id,
                    'status' => config('constants.status.active')
                ]);
            }

            $popular_games = Game::active()->popular()->orderBy('id', 'Desc')->get();

            return view('preview-game', [
                'game' => $game,
                'popular_games' => $popular_games,
            ]);
        }

        return redirect()->route('website.home.page')->with(['error_message' => 'Not Found The Game']);
    }

    public function playGamePage($slugUrl, Request $request) {

        $game = Game::where('slug_url', $slugUrl)->active()->first();

        if ($game) {

            if ($game->status_free == config('constants.status_free.inactive')) {
                // Allow Play Game
                if (isset(session()->get('sess_subscriber')['unique_code'])) {
                } else {
                    return redirect()->back()->with(['error_message' => 'Not Allow To Play The Game']);
                }
            }

            Game::where('slug_url', $slugUrl)
                ->increment('count_plays', 1);

            if (isset(session()->get('sess_subscriber')['unique_code'])) {

                TokenPlayGame::create([
                    'partner_id' => session()->get('sess_subscriber')['partner_id'],
                    'token_id' => session()->get('sess_subscriber')['token_id'],
                    'type' => 'play',
                    'game_id' => $game->id,
                    'status' => config('constants.status.active')
                ]);
            }

            return view('play-game', [
                'game' => $game
            ]);
        }

        return redirect()->route('website.home.page')->with(['error_message' => 'Not Found The Game']);
    }
}


