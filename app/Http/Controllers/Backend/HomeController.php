<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    //
    public function home() {
        if (Auth::check() && Auth::user()->position == config('constants.position.superadmin')) {
            return redirect()->route('backend.report-token-play-games.index');
        } else if (Auth::check() && Auth::user()->position == config('constants.position.admin')) {
            return redirect()->route('backend.report-token-play-games.index');
        }

        return redirect()->route('backend.login');
    }
}
