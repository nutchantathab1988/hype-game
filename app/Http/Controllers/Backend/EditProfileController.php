<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EditProfileController extends Controller
{
    //
    public function edit () {

        return view('backend.pages.profile.edit', [
            'user' => Auth::user()
        ]);
    }

    public function update (Request $request) {

        $validated = $request->validate([
            'name' => 'required',
        ]);

        $data = [
            'name' => $request->input('name')
        ];

        User::where('id', Auth::user()->id)
            ->update($data);

        return redirect()->back()->with(['success' => 'Update Data Completed!']);
    }
}
