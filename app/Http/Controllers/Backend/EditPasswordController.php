<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EditPasswordController extends Controller
{
    //
    public function edit () {

        return view('backend.pages.password.edit');
    }

    public function update (Request $request) {

        $validated = $request->validate([
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);

        $data = [
            'password' => Hash::make($request->input('password'))
        ];

        User::where('id', Auth::user()->id)
            ->update($data);

        return redirect()->back()->with(['success' => 'Update Data Completed!']);
    }
}
