<?php

namespace App\Http\Controllers\Backend;

use App\Exports\ReportTokenPlayGameExport;
use App\Http\Controllers\Controller;
use App\Models\Partner;
use App\Models\TokenPlayGame;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class ReportTokenPlayGameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $partners = Partner::active()->get();

        return view('backend.pages.report-token-play-games.list', [
            'partners' => $partners
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export(Request $request) {

        $validated = $request->validate([
            'partner_id' => 'required',
            'date_range' => 'sometimes',
        ]);

        list($start_date_time, $end_date_time) = convertDateRangeToStartDateTimeEndDateTime($request->input('date_range'));

        $token_play_games = TokenPlayGame::filters($request->input('partner_id'), $start_date_time, $end_date_time)->get();

        return Excel::download(new ReportTokenPlayGameExport($token_play_games), Carbon::now().'_tokens.xls');
    }
}
