<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Token;
use App\Models\GenerateToken;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    // http://localhost/game_portal_project/public/c?t=thECG/MP+BJwJYHshQ1ERA==
    public function convertEncryptedRedirectToHomePage(Request $request) {

        // check param string t
        if ($request->t) {

            $generate_token = GenerateToken::where('unique_code', $request->t)->inactive()->first();

            if ($generate_token) {

                GenerateToken::where('id', $generate_token->id)
                    ->update([
                        'status' => config('constants.status.active')
                    ]);

                $token = Token::create([
                    'partner_id' => $generate_token->id,
                    'unique_code' => $generate_token->unique_code,
                    'status' => config('constants.status.active')
                ]);

                // Store a piece of data in the session...
                $sess_subscriber = [
                    'partner_id' => $generate_token->id,
                    'token_id' => $token->id,
                    'unique_code' => $generate_token->unique_code,
                    'created_at' => Carbon::now(),
                ];

                session([
                    'sess_subscriber' => $sess_subscriber
                ]);
            }
        }

        return redirect()->route('website.home.page');
    }

    public function homePage() {

        $popular_games = Game::active()->popular()->orderBy('id', 'Desc')->get();
        $free_games = Game::active()->free()->orderBy('id', 'Desc')->get();
        $all_games = Game::active()->orderBy('id', 'Desc')->get();

        return view('home', [
            'popular_games' => $popular_games,
            'free_games' => $free_games,
            'all_games' => $all_games
        ]);
    }
}
