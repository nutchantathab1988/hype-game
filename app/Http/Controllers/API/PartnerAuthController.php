<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PartnerAuthController extends Controller
{
    //
    public function login(Request $request) {

        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $partner = Partner::where('username', $request->username)->active()->first();

        if (! $partner || ! Hash::check($request->password, $partner->password)) {

            return response()->json([
                'message' => 'Invalid Credentials'
            ], 401);
        }

        $token = $partner->createToken($partner->code.'-AuthToken')->plainTextToken;

        return response()->json([
            'message' => 'Generate Token Completed!',
            'access_token' => $token,
        ]);
    }

    public function logout(Request $request){

        // Revoke the token that was used to authenticate the current request...
        $request->user()->currentAccessToken()->delete();

        return response()->json([
            'message' => 'Deleted Token Completed!',
        ]);
    }

    public function profile(Request $request) {
        return response()->json([
            'profile' => $request->user()
        ]);
    }
}
