<?php

namespace App\Http\Controllers\API;

use App\Models\GenerateToken;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GenerateTokenController extends Controller
{
    //
    public function generateToken(Request $request) {

        $request->validate([
            'limit' => 'required|numeric',
        ]);

        Log::channel('generate_token')->info("Generate Token to partner code: ".$request->user()->code.", Limit: ".$request->limit);

        $responses = [];

        for ($x=0; $x<$request->limit; $x++) {

            // $encrypted_string = openssl_encrypt(uniqid()."|".$request->user()->id, config('constants.code'), config('constants.secretKey'));

            $unique_code = generateRandomString();

            GenerateToken::create([
                'partner_id' => $request->user()->id,
                'unique_code' => $unique_code,
            ]);

            array_push($responses, $unique_code);
        }

        Log::channel('generate_token')->info(json_encode($responses, JSON_PRETTY_PRINT));

        return response()->json([
            'message' => 'Genearte Token Completed!',
            'data' => [
                $responses
            ]
        ]);
    }
}
