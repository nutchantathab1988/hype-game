<?php
function hasPermission($auth_permission, $permissions)
{
    $permissions_array = explode('|', $permissions);
    foreach($permissions_array as $permission) {
        if ($auth_permission == $permission) {
            return true;
        }
    }

    return false;
}

// YYYY-MM-DD - YYYY-MM-DD, [YYYY-MM-DD HH:MM:SS, YYYY-MM-DD HH:MM:SS]
function convertDateRangeToStartDateTimeEndDateTime($date_range)
{
    if ($date_range) {

        $array_date_range = explode(" - ", $date_range);

        return [$array_date_range[0]. " 00:00:00", $array_date_range[1]." 23:59:59"];
    }

    return null;
}

function generateRandomString($length = 12) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }
    return $randomString;
}
