<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->string('slug_url')->unique();
            $table->json('category');
            $table->string('name');
            $table->text('description')->nullable();
            $table->string('status')->default(config('constants.status.active'));
            $table->string('status_free')->default(config('constants.status_free.active'));
            $table->string('status_popular')->default(config('constants.status_popular.active'));
            $table->integer('rating');
            $table->integer('count_views');
            $table->integer('count_plays');
            $table->string('path_game');
            $table->string('thumbnail_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
