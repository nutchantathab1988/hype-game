<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTokenPlayGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('token_play_games', function (Blueprint $table) {
            $table->id();
            $table->integer('partner_id');
            $table->integer('token_id');
            $table->integer('game_id');
            $table->string('type');
            $table->string('status')->default(config('constants.status.active'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('token_play_games');
    }
}
