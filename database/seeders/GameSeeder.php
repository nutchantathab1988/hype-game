<?php

namespace Database\Seeders;

use App\Models\Game;
use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (config("games.all_games") as $game) {

            // if ($game['status']) {
            //     $status = config('constants.status.active');
            // } else {
            //     $status = config('constants.status.inactive');
            // }

            // if ($game['status_free']) {
            //     $status_free = config('constants.status_free.active');
            // } else {
            //     $status_free = config('constants.status_free.inactive');
            // }

            // if ($game['status_popular']) {
            //     $status_popular = config('constants.status_popular.active');
            // } else {
            //     $status_popular = config('constants.status_popular.inactive');
            // }

            Game::create([
                'slug_url' => $game['slug_url'],
                'category' => $game['category'],
                'name' => $game['name'],
                'description' => $game['description'],
                'status' => $game['status'],
                'status_free' => $game['status_free'],
                'status_popular' => $game['status_popular'],
                'rating' => $game['rating'],
                'count_views' => $game['count_views'],
                'count_plays' => $game['count_plays'],
                'path_game' => $game['path_game'],
                'thumbnail_image' => $game['thumbnail_image'],
            ]);
        }
    }
}
