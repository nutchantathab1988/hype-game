<?php

namespace Database\Seeders;

use App\Models\Partner;
use Illuminate\Database\Seeder;

class PartnerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($x=1; $x<=5; $x++) {

            Partner::create([
                'code' => 'Demo'.$x,
                'name' => 'Demo'.$x,
                'username' => 'demo'.$x,
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'status' => config('constants.status.active'),
                'user_created_id' => 1
            ]);
        }
    }
}
