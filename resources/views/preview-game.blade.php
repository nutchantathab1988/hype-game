@extends('layouts.template')
@section('title', 'เกมออนไลน์ Hype Game - '.$game->name)
@section('content')

<div id="content">
    <section class="single-project-section">
        <div class="container">
            <div class="title-project">
                <h1>{{ $game->name }}</h1>
            </div>
            <div class="project-gallery">
                <div class="flexslider">
                    <ul class="slides" style="list-style: none; padding: 0px;">
                        <li>
                            <img alt="" src="{{ asset('games/'.$game->thumbnail_image) }}" />
                        </li>
                    </ul>
                </div>
                {{-- <a class="likes" href="#"><i class="fa fa-heart-o"></i><span>15</span></a> --}}
            </div>
            <div class="project-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2>About the game</h2>
                        <p>{{ $game->description }}</p>
                        <div>
                            <a class="viewgame" href="{{ route("website.play.game.page", ['slugUrl' => $game->slug_url]) }}"><i class="fa fa-play"></i><span style="padding-left:10px;">PLALY NOW</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End single-project -->

</div>
<!-- end content -->

<!-- shop-section2
    ================================================== -->
<section id="shop-section2">
    <div class="container">
        <div class="tab-product-box">
            <div class="title-line">
            <h1>Popular Games<a href="{{ route('website.all.game.page', ['status_popular' => config('constants.status_popular.active')]) }}">View All</a></h1>
            </div>
            <!-- Nav tabs -->
            <div class="tab-posts-box">
                <div class="tab-content">
                    <div class="tab-pane active" id="option1">
                        <div class="row">
                            @foreach ($popular_games as $popular_game)
                                <div class="col-md-4">
                                    <ul class="product-list">
                                        <li>
                                            <img src="{{ asset('games/'.$popular_game->thumbnail_image) }}" alt="" width="100px">
                                            <div class="product-list-content">
                                                <h2><a href="{{ route('website.preview.game.page', ['slugUrl' => $popular_game->slug_url]) }}">{{ $popular_game->name_limit }}</a></h2>
                                                <p>
                                                    @for ($start_rating=1; $start_rating<=5; $start_rating++)
                                                        @if ($start_rating <= $popular_game->rating)
                                                            <i class="fa fa-star"></i>
                                                        @else
                                                            <i class="fa fa-star-o"></i>
                                                        @endif
                                                    @endfor
                                                </p>
                                                <span><i class="fa fa-eye"></i> {{ number_format($popular_game->count_views) }}<i class="fa fa-user" style="padding-left: 15px;"></i> {{ number_format($popular_game->count_plays) }}</span>
                                                <div class="product-links">
                                                    <a class="add-cart-btn" href="{{ route('website.preview.game.page', ['slugUrl' => $popular_game->slug_url]) }}"><i class="fa fa-play"></i><span>PLALY NOW</span></a>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End shop section2 -->
@endsection
