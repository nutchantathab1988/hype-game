@extends('layouts.template')
@section('title', 'เกมออนไลน์ Hype เกมส์- เล่นเลยตอนนี้!')
@section('content')
    <br/>
    <br/>

    <div id="shop-section" class="item-center">
        <iframe src="{{ asset('assets/magazine/index.html') }}" title="TOP IMAGE"></iframe>
    </div>

    <!-- shop-section2
        ================================================== -->
    <section id="shop-section2">
        <div class="container">
            <div class="tab-product-box">
                <div class="title-line">
                    <h1>All Games<a href="#">View All</a></h1>
                </div>
                <!-- Nav tabs -->
                <div class="tab-posts-box">
                    <div class="tab-content">
                        <div class="tab-pane active" id="option1">
                            <div class="row">
                                @foreach ($all_games as $all_game)
                                    <div class="col-md-4">
                                        <ul class="product-list">
                                            <li>
                                                <img src="{{ asset('games/'.$all_game->thumbnail_image) }}" alt="" width="100px">
                                                <div class="product-list-content">
                                                    <h2><a href="{{ route('website.preview.game.page', ['slugUrl' => $all_game->slug_url]) }}">{{ $all_game->name_limit }}</a></h2>
                                                    <p>
                                                        @for ($start_rating=1; $start_rating<=5; $start_rating++)
                                                            @if ($start_rating <= $all_game->rating)
                                                                <i class="fa fa-star"></i>
                                                            @else
                                                                <i class="fa fa-star-o"></i>
                                                            @endif
                                                        @endfor
                                                    </p>
                                                    <span><i class="fa fa-eye"></i> {{ number_format($all_game->count_views) }}<i class="fa fa-user" style="padding-left: 15px;"></i> {{ number_format($all_game->count_plays) }}</span>
                                                    <div class="product-links">
                                                        <a class="add-cart-btn" href="{{ route('website.preview.game.page', ['slugUrl' => $all_game->slug_url]) }}"><i class="fa fa-play"></i><span>PLALY NOW</span></a>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End shop section2 -->
@endsection
