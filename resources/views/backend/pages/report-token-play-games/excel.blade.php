<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Token Play Games</title>
</head>
<body>
    <table width="100%" border="1">
        <thead>
            <tr>
                <th width="5" align="center" style="border: 1px solid black; background-color:antiquewhite"><b>No.</b></th>
                <th width="20" align="center" style="border: 1px solid black; background-color:antiquewhite"><b>Partner</b></th>
                <th width="20" align="center" style="border: 1px solid black; background-color:antiquewhite"><b>Token</b></th>
                <th width="20" align="center" style="border: 1px solid black; background-color:antiquewhite"><b>Type</b></th>
                <th width="20" align="center" style="border: 1px solid black; background-color:antiquewhite"><b>Game</b></th>
                <th width="20" align="center" style="border: 1px solid black; background-color:antiquewhite"><b>Created At</b></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($token_play_games as $key_token_play_game => $token_play_game)
                <tr>
                    <td style="border: 1px solid black;" align="right">{{ $key_token_play_game+1 }}</td>
                    <td style="border: 1px solid black;">{{ $token_play_game->partner->code }} {{ $token_play_game->partner->name }}</td>
                    <td style="border: 1px solid black;">{{ $token_play_game->token->unique_code }}</td>
                    <td style="border: 1px solid black;">{{ $token_play_game->type }}</td>
                    <td style="border: 1px solid black;">{{ $token_play_game->game->name }}</td>
                    <td style="border: 1px solid black;" align="right">{{ $token_play_game->created_at }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>
