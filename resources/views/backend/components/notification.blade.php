@if($errors->any())
    <div class="alert alert-danger">
        <p><strong>Opps Something went wrong {{ \Carbon\Carbon::now() }}</strong></p>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('success'))
    <div class="alert alert-success">{{ session('success') }} {{ \Carbon\Carbon::now() }}</div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        <p><strong>Opps Something went wrong {{ \Carbon\Carbon::now() }}</strong></p>
        <ul>
            <li>{{ session('error') }}</li>
        </ul>
    </div>
@endif
