            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="float-right d-none d-sm-block">
                    <b>Version</b> 1.1.0
                </div>
                <strong>Copyright &copy; 2014-2021 <a href="#">{{ config('app.name') }}</a>.</strong> All rights reserved.
            </footer>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
        </div>
        <!-- ./wrapper -->

        <!-- jQuery -->
        <script src="{{ asset("assets/adminlte/plugins/jquery/jquery.min.js") }}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{ asset("assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js") }}"></script>
        <!-- Select2 -->
        <script src="{{ asset("assets/adminlte/plugins/select2/js/select2.full.min.js") }}"></script>
        <!-- Bootstrap4 Duallistbox -->
        <script src="{{ asset("assets/adminlte/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js") }}"></script>
        <!-- InputMask -->
        <script src="{{ asset("assets/adminlte/plugins/moment/moment.min.js") }}"></script>
        <script src="{{ asset("assets/adminlte/plugins/inputmask/jquery.inputmask.min.js") }}"></script>
        <!-- date-range-picker -->
        <script src="{{ asset("assets/adminlte/plugins/daterangepicker/daterangepicker.js") }}"></script>
        <!-- bootstrap color picker -->
        <script src="{{ asset("assets/adminlte/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js") }}"></script>
        <!-- Tempusdominus Bootstrap 4 -->
        <script src="{{ asset("assets/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js") }}"></script>
        <!-- Bootstrap Switch -->
        <script src="{{ asset("assets/adminlte/plugins/bootstrap-switch/js/bootstrap-switch.min.js") }}"></script>
        <!-- BS-Stepper -->
        <script src="{{ asset("assets/adminlte/plugins/bs-stepper/js/bs-stepper.min.js") }}"></script>
        <!-- dropzonejs -->
        <script src="{{ asset("assets/adminlte/plugins/dropzone/min/dropzone.min.js") }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset("assets/adminlte/dist/js/adminlte.min.js") }}"></script>
        <!-- AdminLTE for demo purposes -->
        <!--<script src="{{ asset("assets/adminlte/dist/js/demo.js") }}"></script>-->
        <!-- Page specific script -->
        <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
            theme: 'bootstrap4'
            })

            //Date range picker
            $('#reservation').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY-MM-DD',
                    cancelLabel: 'Clear'
                }
            })

            $('#reservation').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
            });

            $('#reservation').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        })
        </script>

        @yield('script')
    </body>
</html>
