<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }} | @yield('title')</title>
    <!-- Google Font:  Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/fontawesome-free/css/all.min.css") }}">
    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/daterangepicker/daterangepicker.css") }}">
    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css") }}">
    <!-- Bootstrap Color Picker -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css") }}">
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css") }}">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/select2/css/select2.min.css") }}">
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css") }}">
    <!-- Bootstrap4 Duallistbox -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css") }}">
    <!-- BS Stepper -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/bs-stepper/css/bs-stepper.min.css") }}">
    <!-- dropzonejs -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/plugins/dropzone/min/dropzone.min.css") }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("assets/adminlte/dist/css/adminlte.min.css") }}">
    @yield('style')
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Home</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="#" class="nav-link">Contact</a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="ml-auto navbar-nav">
                <li class="nav-item dropdown user user-menu">
                    <a href="#" class="text-white nav-link dropdown-toggle btn btn-dark" data-toggle="dropdown">
                        <span class="hidden-xs">Setting</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="text-center">
                                <div class="pull-left" style="display:inline">
                                    <a href="{{ route('backend.edit.profile') }}" class="btn btn-info btn-flat">Profile</a>
                                </div>
                                <div class="pull-center" style="display:inline">
                                    <a href="{{ route('backend.edit.password') }}" class="btn btn-warning btn-flat">Password</a>
                                </div>
                                <div class="pull-right" style="display:inline">

                                    <a class="btn btn-danger btn-flat" href="{{ route('backend.logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        Sign out
                                    </a>

                                    <form id="logout-form" action="{{ route('backend.logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

    @include('backend.layouts.sidebar')

    @yield('content')
