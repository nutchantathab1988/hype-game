@extends('layouts.template')
@section('title', 'เกมออนไลน์ Hype เกมส์- เล่นเลยตอนนี้!')
@section('content')
    <br/>
    <br/>
    <div id="shop-section" class="item-center">
        <iframe src="{{ asset('assets/magazine/index.html') }}" title="TOP IMAGE"></iframe>
    </div>

	<!-- shop-section
		================================================== -->
	<div id="shop-section">
		<div class="container">
			<div class="title-section">
				<h1>Most Popular Games</h1>
				<span></span>
			</div>
			<div class="title-line">
				<h1>Top<a href="{{ route('website.all.game.page', ['status_popular' => config('constants.status_popular.active')]) }}">View All</a></h1>
			</div>
			<div class="products-box">
				<div class="carousel-arrows owl-arrows">
					<a href="#" class="prev-link"><i class="fa fa-chevron-left"></i></a>
					<a href="#" class="next-link"><i class="fa fa-chevron-right"></i></a>
				</div>
				<div id="owl-demo-pro" class="owl-carousel">
                    @foreach ($popular_games as $popular_game)
                        <div class="item product-post">
                            <h2><a href="{{ route('website.preview.game.page', ['slugUrl' => $popular_game->slug_url]) }}">{{ $popular_game->name }}</a></h2>
                            <p>
                                @for ($start_rating=1; $start_rating<=5; $start_rating++)
                                    @if ($start_rating <= $popular_game->rating)
                                        <i class="fa fa-star"></i>
                                    @else
                                        <i class="fa fa-star-o"></i>
                                    @endif
                                @endfor
                            </p>
                            <span><i class="fa fa-eye"></i> {{ number_format($popular_game->count_views) }}<i class="fa fa-user" style="padding-left: 15px;"></i> {{ number_format($popular_game->count_plays) }}</span>
                            <div class="gal-product">
                                <img src="{{ asset('games/'.$popular_game->thumbnail_image) }}" alt="" width="50px">
                            </div>
                            <div class="product-links">
                                <a class="add-cart-btn" href="{{ route('website.preview.game.page', ['slugUrl' => $popular_game->slug_url]) }}"><i class="fa fa-play"></i><span>PLALY NOW</span></a>
                            </div>
                        </div>
                    @endforeach
				</div>
			</div>
		</div>
		<!-- shop-section -->

		<!-- shop-section2
			================================================== -->
		<section id="shop-section2">
			<div class="container">
				<div class="tab-product-box">
					<div class="title-line">
					    <h1>Free Games<a href="{{ route('website.all.game.page', ['status_free' => config('constants.status_free.active')]) }}">View All</a></h1>
					</div>
					<!-- Nav tabs -->
					<div class="tab-posts-box">
						<div class="tab-content">
							<div class="tab-pane active" id="option1">
								<div class="row">
                                    @foreach ($free_games as $free_game)
                                        <div class="col-md-4">
                                            <ul class="product-list">
                                                <li>
                                                    <img src="{{ asset('games/'.$free_game->thumbnail_image) }}" alt="" width="100px">
                                                    <div class="product-list-content">
                                                        <h2><a href="{{ route('website.preview.game.page', ['slugUrl' => $free_game->slug_url]) }}">{{ $free_game->name_limit }}</a></h2>
                                                        <p>
                                                            @for ($start_rating=1; $start_rating<=5; $start_rating++)
                                                                @if ($start_rating <= $free_game->rating)
                                                                    <i class="fa fa-star"></i>
                                                                @else
                                                                    <i class="fa fa-star-o"></i>
                                                                @endif
                                                            @endfor
                                                        </p>
                                                        <span>
                                                        <i class="fa fa-eye"></i> {{ number_format($free_game->count_views) }}<i class="fa fa-user" style="padding-left: 15px;"></i> {{ number_format($free_game->count_plays) }}</span>
                                                        <div class="product-links">
                                                            <a class="add-cart-btn" href="{{ route('website.preview.game.page', ['slugUrl' => $free_game->slug_url]) }}"><i class="fa fa-play"></i><span>PLALY NOW</span></a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    @endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End shop section2 -->

		<!-- shop-section2
			================================================== -->
		<section id="shop-section2">
			<div class="container">
				<div class="tab-product-box">
					<div class="title-line">
					    <h1>All Games<a href="{{ route('website.all.game.page') }}">View All</a></h1>
					</div>
					<!-- Nav tabs -->
					<div class="tab-posts-box">
						<div class="tab-content">
							<div class="tab-pane active" id="option1">
								<div class="row">
                                    @foreach ($all_games as $all_game)
                                        <div class="col-md-4">
                                            <ul class="product-list">
                                                <li>
                                                    <img src="{{ asset('games/'.$all_game->thumbnail_image) }}" alt="" width="100px">
                                                    <div class="product-list-content">
                                                        <h2><a href="{{ route('website.preview.game.page', ['slugUrl' => $all_game->slug_url]) }}">{{ $all_game->name_limit }}</a></h2>
                                                        <p>
                                                            @for ($start_rating=1; $start_rating<=5; $start_rating++)
                                                                @if ($start_rating <= $all_game->rating)
                                                                    <i class="fa fa-star"></i>
                                                                @else
                                                                    <i class="fa fa-star-o"></i>
                                                                @endif
                                                            @endfor
                                                        </p>
                                                        <span><i class="fa fa-eye"></i> {{ number_format($all_game->count_views) }}<i class="fa fa-user" style="padding-left: 15px;"></i> {{ number_format($all_game->count_plays) }}</span>
                                                        <div class="product-links">
                                                            <a class="add-cart-btn" href="{{ route('website.preview.game.page', ['slugUrl' => $all_game->slug_url]) }}"><i class="fa fa-play"></i><span>PLALY NOW</span></a>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    @endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- End shop section2 -->
@endsection
