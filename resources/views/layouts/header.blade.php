<!doctype html>
<html lang="en" class="no-js">
    <head>
        <title>Hype Games | @yield('title') </title>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" value="ค้นพบโลกแห่งเกมออนไลน์ เล่นได้ทันที, Hype เกมส์,  ไม่ต้องดาวน์โหลด, เกม, เกมส์, เกมออนไลน์, เกมออนไลน์ฟรี, เกมฟรี, เล่นเกมฟรี, เล่นเกมออนไลน์, เล่นเกม, และสนุกไปกับเกมที่สามารถเล่นได้กับทุกอุปกรณ์">
        <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"> -->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/bootstrap.min.css') }}" media="screen">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.css') }}" media="screen">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.theme.css') }}" media="screen">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.bxslider.css') }}" media="screen">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/magnific-popup.css') }}" media="screen">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.css') }}" media="screen">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/animate.css') }}" media="screen">
        <!-- REVOLUTION BANNER CSS SETTINGS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/settings.css') }}" media="screen">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}" media="screen">

        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('/apple-icon-120x120.png') }}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('/apple-icon-144x144.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/apple-icon-180x180.png') }}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('/android-icon-192x192.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('/favicon-96x96.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset("/manifest.json") }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('/ms-icon-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">

        @yield('style')

        <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.migrate.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/owl.carousel.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.bxslider.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.appear.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.countTo.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.imagesloaded.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.isotope.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/retina-1.1.0.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/plugins-scroll.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/smooth-scroll.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/waypoint.min.js') }}"></script>
        <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
        <script type="text/javascript" src="{{ asset('assets/js/jquery.themepunch.tools.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.themepunch.revolution.min.js') }}"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="{{ asset('assets/js/gmap3.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/script.js') }}"></script>
        <!-- Sweet Alert! -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    </head>
    <body>
        <!-- Container -->
        <div id="container">
            <!-- Header
                ================================================== -->
            <header class="clearfix">
                <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                    <div class="container">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand fix-color" href="index.html">HYPE<span>GAMES</span></a>
                        </div>

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right navigate-section">
                                <li><a href="{{ route('website.home.page') }}" class="@if(Route::is('website.home.page')) active @endif">HOME</a></li>
                                <li><a href="{{ route('website.all.game.page', ['category' => 'puzzle']) }}">PUZZLE</a></li>
                                <li><a href="{{ route('website.all.game.page', ['category' => 'educational']) }}">EDUCATION</a></li>
                                <li><a href="{{ route('website.all.game.page', ['category' => 'entertainmemnt']) }}">ENTERTAINMENT</a></li>
                                <li class="megadrop"><a class="" href="#home-section" class="@if(Route::is('website.all.game.page')) active @endif">MORE GAMES</a>
                                    <div class="megadrop-down">
                                        <div class="dropdown">
                                            <div class="row">
                                                <div class="col-md-8 col-sm-8">
                                                    <div class="row">
                                                        <div class="col-md-4 col-sm-4">
                                                            <ul>
                                                                <li><a href="{{ route('website.all.game.page', ['category' => 'action']) }}">ACTION/ADV</a></li>
                                                                <li><a href="{{ route('website.all.game.page', ['category' => 'shooter']) }}">SHOOTER</a></li>
                                                                <li><a href="{{ route('website.all.game.page', ['category' => 'sport']) }}">SPORT</a></li>
                                                                <li><a href="{{ route('website.all.game.page', ['category' => 'strategy']) }}">STRATEGY</a></li>
                                                                <li><a href="{{ route('website.all.game.page', ['category' => 'arcade']) }}">ARCADE</a></li>
                                                                {{-- <li><a href="{{ route('website.all.game.page', ['category' => '']) }}">OTHER</a></li> --}}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div><!-- /.container-fluid -->
                </nav>
            </header>
            <!-- End Header -->

            <!-- End home section -->

