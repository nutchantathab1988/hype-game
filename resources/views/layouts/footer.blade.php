    <!-- footer
			================================================== -->
            <footer>
                <div class="container">
                    <p class="copyright">
                        &copy; Copyright 2024. "Hype Games" By BNINE Tech Solutions All rights reserved.
                    </p>
                </div>
            </footer>
            <!-- End footer -->

        </div>
        <!-- End Container -->

        <!-- Revolution slider -->
        <script type="text/javascript">

            jQuery(document).ready(function() {

                jQuery('.tp-banner').show().revolution(
                {
                    dottedOverlay:"none",
                    delay:10000,
                    startwidth:960,
                    startheight:680,
                    hideThumbs:200,

                    thumbWidth:100,
                    thumbHeight:50,
                    thumbAmount:5,

                    navigationType:"bullet",

                    touchenabled:"on",
                    onHoverStop:"off",

                    swipe_velocity: 0.7,
                    swipe_min_touches: 1,
                    swipe_max_touches: 1,
                    drag_block_vertical: false,

                    parallax:"mouse",
                    parallaxBgFreeze:"on",
                    parallaxLevels:[7,4,3,2,5,4,3,2,1,0],

                    keyboardNavigation:"off",

                    navigationHAlign:"center",
                    navigationVAlign:"bottom",
                    navigationHOffset:0,
                    navigationVOffset:120,
                    soloArrowLeftHOffset:250,
                    soloArrowRightHOffset:250,
                    shadow:0,
                    fullWidth:"off",

                    spinner:"spinner4",

                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,

                    shuffle:"off",

                    autoHeight:"off",
                    forceFullWidth:"off",

                    hideThumbsOnMobile:"off",
                    hideNavDelayOnMobile:1500,
                    hideBulletsOnMobile:"off",
                    hideArrowsOnMobile:"off",
                    hideThumbsUnderResolution:0,

                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    startWithSlide:0,
                    fullScreenOffsetContainer: ".header"
                });

            });	//ready

            @if($message = session()->get('error_message'))
                Swal.fire({
                    icon: "error",
                    title: 'Oops...',
                    text: '{{ session()->get('error_message') }}',
                })
            @endif
        </script>
    </body>
</html>
