<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PartnerAuthController;
use App\Http\Controllers\API\GenerateTokenController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['prefix' => 'auth'], function() {

    Route::post('login', [PartnerAuthController::class, 'login']);

    Route::group(['middleware' => 'auth:sanctum'], function() {
        Route::get('profile', [PartnerAuthController::class, 'profile']);
        Route::post('logout', [PartnerAuthController::class, 'logout']);
    });
});

Route::group(['middleware' => 'auth:sanctum'], function() {
    Route::post('generateToken', [GenerateTokenController::class, 'generateToken']);
});



// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
