<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Backend\EditPasswordController;
use App\Http\Controllers\Backend\EditProfileController;
use App\Http\Controllers\Backend\HomeController;
use App\Http\Controllers\Backend\ManageCourseController;
use App\Http\Controllers\Backend\ManageUserController;
use App\Http\Controllers\Backend\ReportTokenPlayGameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Backend
Route::group(['prefix' => 'backend', 'as' => 'backend.'], function() {

    // Auth
    Auth::routes();

    // After Login go to Home
    Route::get('/', [HomeController::class, 'home']);

    // Edit Profile
    Route::get('edit-profile', [EditProfileController::class, 'edit'])->name('edit.profile');
    Route::post('update-profile', [EditProfileController::class, 'update'])->name('update.profile');

    // Edit Password
    Route::get('edit-password', [EditPasswordController::class, 'edit'])->name('edit.password');
    Route::post('update-password', [EditPasswordController::class, 'update'])->name('update.password');

    // Middleware Check SuperAdmin
    Route::group(['middleware' => ['checkAuthUser:superadmin']], function () {
        // Route::resource('/manage-users', ManageUserController::class);
    });

    // Middleware Check SuperAdmin or Admin
    Route::group(['middleware' => ['checkAuthUser:superadmin|admin']], function () {
        // Report Token Access & Play Games
        Route::resource('/report-token-play-games', ReportTokenPlayGameController::class);
        Route::post('/report-token-play-games-export', [ReportTokenPlayGameController::class, "export"])->name('report.token.play.games.export');
    });
});

// Website Game Portal
Route::group(['prefix' => '', 'as' => 'website.'], function () {

    // Check Encrypted String, Redirect to Home Page
    Route::get('/c', [App\Http\Controllers\HomeController::class, 'convertEncryptedRedirectToHomePage']);
    // Home Page
    Route::get('', [App\Http\Controllers\HomeController::class, 'homePage'])->name('home.page');
    // All Game Page
    Route::get('/allgames', [App\Http\Controllers\GameController::class, 'allGamePage'])->name('all.game.page');
    // Preview Game Page
    Route::get('/preview/{slugUrl}', [App\Http\Controllers\GameController::class, 'previewGamePage'])->name('preview.game.page');
    // All Play Game Page
    Route::get('/play/{slugUrl}', [App\Http\Controllers\GameController::class, 'playGamePage'])->name('play.game.page');

    Route::get('/check/session', function() {
        return session()->all();
    });

    Route::get('/delete/session', function() {
        return session()->flush();
    });
});

